## AWS Amplify - GraphQL API


# CLI Commands

```bash

mkdir amplify-graphql

cd amplify-graphql

npm install -g @aws-amplify/cli

amplify configure

amplify init

amplify add api

amplify push

```


# GraphQL Mutations


```graphql
# Create a new Bolg
mutation CreateBlog {
  createBlog(input: {
    name: "My New Blog!"
  }) {
    id
    name
  }
}
```


```graphql
# Create a new Post for a Blog
mutation CreatePost($blogId:String!) {
  createPost(input:{title:"My Post!", blogID: $blogId}) {
    id
    title
    blog {
      id
      name
    }
  }
}

# Variables

{
"blogId":"18cc2e9b-9337-4b1f-ad69-a0ee955015a3"
}

```


```graphql
# Create a new comment for a Post
mutation CreateComment($postId:ID!) {
  createComment(input:{content:"A comment!", postID:$postId}) {
    id
    content
    post {
      id
      title
      blog {
        id
        name
      }
    }
  }
}

# Variables

{
"postId":"82a24791-9fca-4041-96a5-0c9f0d08d70a"
}

```


# GraphQL Queries


```graphql
# List all blogs, their posts, and their posts comments.

query ListBlogs {
  listBlogs {
    items {
      id
      name
      posts {
        items {
          id
          title
          comments {
            items {
              id
              content
            }
          }
        }
      }
    }
  }
}

```
